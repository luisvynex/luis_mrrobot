package luis_robots;
import robocode.*;
import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import java.awt.*;

/*Um excelente robô para 1x1 e mediano para desafios em grupo, visando a simplicidade no
código e a eficiência nos desafios.*/

public class LuisMrRobot extends AdvancedRobot {
	public void run() {
	
	setBodyColor(Color.black);
	setGunColor(Color.white);
	setRadarColor(Color.red);
	setScanColor(Color.red);
	setBulletColor(Color.white);
	
		while (true) {
		setTurnRight(10000); //vira para a esquerda
		setMaxVelocity(5); //velocidade max do robô
		ahead(10000); //anda pra frente
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(2); //Perde 2 de vida
		
	}
		//Quando meu robô bater em outro
		public void onHitRobot(HitRobotEvent e) {
			//se o ângulo em graus do inimigo estiver entre -10 e 10
			if (e.getBearing() > -10 && e.getBearing() < 10) {
				fire(3);
		}
			//se eu bati em um robô
			if (e.isMyFault()) {
			turnRight(10);
						}
			}

	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	
}
