## LuisMrRobot

#### Explicação:
O LuisMrRobot é um robô bastante agressivo por conta do dano causado e vida sacrificada, têm um excelente desempenho no 1x1 mas sofre um pouco dependendo do posicionamento inicial em desafios com mais de um inimigo.
O movimento em looping foi feito com uma rotação para a direita em conjunto com o movimento para frente de forma simultânea e uma redução de velocidade de até 5 píxels, se for encontrado outro robô, o LuisMrRobot dispara um projétil com 2 de custo de vida, caso haja impacto entre o LuisMrRobot e outro robô, ele andará 10 pixels para a direita, buscará o ângulo do robô inimigo, verificará o posicionamento para um melhor ajuste de mira (>-10 e <10) e dispara um projétil com 3 de custo de vida causando um dano maior já que dificilmente errará o projétil.

#### Pontos fortes:
- Quando bem posicionado o LuisMrRobot têm um ótimo desempenho
- É um robô bem agressivo, então consegue decidir um round com poucos tiros acertados.
- Como o custo de vida de cada projétil é alto, a recuperação de vida também é muito alta e pode trazer o robô de volta pro jogo com poucos disparos


#### Pontos fracos:
- Quando o robô inicia nas paredes, sofre um pouco pra poder se posicionar e acaba perdendo bastante vida
- Têm grande dificuldades contra inimigos com mobilidade alta e movimentos desordenados
- Como o custo de vida é alto em cada disparo, o robô não pode errar muitos tiros pois pode influenciar no round de forma negativa

## Considerações pessoais
Foi a primeira vez que tive contato com o Robocode, pesquisei durante horas, assisti aulas, vi campeonatos da Unicamp de vários anos e várias etapas diferentes, estudei táticas e os comandos do sourcecode, foi uma experiência muito enriquecedora, divertida e gratificante, consegui utilizar de minha criatividade e obter um resultado relativamente bom. Sem dúvidas, o que eu mais gostei nessa experiência foi ser exposto ao novo e poder aprender algo tão divertido, eu conto como um passo a mais que estou dando em minha carreira que apenas iniciou.